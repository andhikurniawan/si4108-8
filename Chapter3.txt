CHAPTER 3
RANCANGAN SISTEM

3.1 Gambaran Umum Aplikasi
Aplikasi Sparkuy adalah suatu aplikasi yang berguna untuk mewadahi para olahragawan untuk melalukan booking pelatihan olahraga, mencari komunitas untuk berolahraga, dan menemukan trainer dan lokasi olahraga. Aplikasi ini diharapkan mampu memberikan kemudahan kepada olahragawan dan pecinta olahraga lainnya dalam melakukan aktivitas berolahraga
Aplikasi dapat digunakan melalui smartphone dengan Sistem Operasi Android. Aplikasi ini  dibangun  dengan  menggunakan menggunakan   perancangan Unified   Modelling   Language (UML)   karena dibangun dengan pemrograman berorientasi object.Pada  sisi  server  terdapat web  service yang  dibangun  dengan  bahasa pemrograman PHP. Selain itu, terdapat MySQL yang berfungsi sebagai sistem manajemen database.


3.2 Proses Bisnis Aplikasi
Untuk alur proses bisnis Aplikasi Sparkuy, tidak jauh beda dengan aplikasi yang tersedia di PlaySore maupun Appstore.
Ketika Aplikasi sudah terinstal dan akan digunakan oleh pengguna, alur proses yang terjadi adalah :
a. Pertama, pengguna akan diminta membuat akun. Ada juga pilihan login bagi yang sudah memiliki akun, ada juga pilihan login atau sign up dengan akun google atau facebook.
b. Setelah memiliki akun, ada beberapa aktivitas pilihan yang dapat dilakukan. Namun, hal pertama yang muncul setelah login adalah masuk ke menu utama.
c. Di menu utama terdapat deskripsi dari aplikasi, produk-produk, dan berita info lomba.
d. Apabila pengguna memilih produk, maka akan langsung masuk ke produk yang dipilih, dan mengikuti alur dan ketentuan yang sesuai dengan produk yang dipilih.
e. Setelah selesai, pengguna bisa keluar dari aplikasi.

3.3.1 SRS Non Functional 
Kebutuhan non-functional digunakan dalam menganalisis interaksi aplikasi diluar kebutuhan functional. Berikut merupakan beberapa bagian dari kebutuhan non-functional adalah:
-Jaringan Internet:
 Karena untuk membangun sebuah aplikasi tentunya akan dibutuhkan jaringan internet untuk mendukung segala fungsi yang terdapat pada sebuah aplikasi
- Hardware:
  Suatu sistem tentunya membutuhkn hardware untuk menjalankan aplikasi tersebut
- User:
  Sistem membutuhkan user yang mampu mengoperasikan aplikasi yang telah dirancang
Jadi secara keseluruhan Kebutuhan Non-Functional juga memiliki fungsi dan peranan penting dalam merancang dan membuat sebuah aplikasi agar dapat mendukung kebutuhan user.

3.3.2 System Requirement Specification(SRS)
System  Requirement  Specification (SRS)  adalah  spesifikasi  dari  apa yang harus diimplementasikan dan mendeskripsikan bagaimana sistem harus bekerja atau bagian-bagian yang ada di dalam sistem, dapat juga menjadi batasan dalam peoses pengembangan sistem.
System  requirement  specification  functional adalah penjelasan   tentang   layanan   yang   perlu   disediakan   oleh   sistem, bagaimana  menerima  dan  mengolah  masukkan  dan  bagaimana  sistem mengatasi  situasi-situasi  tertentu.  Selain  itu, requirementjuga  secara jelas   menentukan   apa   yang   tidak   dikerjakan   oleh   sistem.   SRS functional menggambarkan system requirement secara detail
Salah satu SRS Functional adalah menampilkan form register dan login yang dapat diakses oleh seluruh user, juga menampilkan kategori olahraga yang dapat diakses oleh semua user

Sistem Operasi : Microsoft Windows 10
Database Management System (DBMS) : SQLite
Pengolah kata : Microsoft Word
Bahasa Pemgrograman : Java
Presentasi : Microsoft Powerpoint
Pengolah Jadwal : Microsoft Project

3.4 Use Case Diagram
Use Case Diagram atau diagram use case merupakan pemodelan untuk menggambarkan kelakuan (behavior) sistem yang akan dibuat. 
Diagram use case mendeskripsikan sebuah interaksi antara satu atau lebih aktor dengan sistem yang akan dibuat
Singkatnya, Diagram use case digunakan untuk mengetahui fungsi apa saja yang ada di dalam sebuah sistem dan siapa saja yang berhak menggunakan fungsi-fungsi tersebut.
Aktor disini ialah Penyedia Lapangan, Pelatih, dan Pemain.